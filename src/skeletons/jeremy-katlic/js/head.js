let CreativeTemplate = {

	/*****
    	PROPERTIES
    *****/
    
    "KeyCodes": { 'tab': 'Tab', 'enter': 'Enter', 'esc': 'Escape', 'space': 'Space', 'end': 'End', 'home': 'Home', 'left': 'ArrowLeft', 'up': 'ArrowUp', 'right': 'ArrowRight', 'down': 'ArrowDown' },
    "IsMyViewPage": false, // UPDATES IN SetTemplateProps METHOD
    "DefaultLogoSrc": '',
    "DropdownTimeout": '', //Will store the CustomDropdown timeout object that will get passed among the dropdown functions.
    "DynamicStyles": '', //This can be added to and if it's not empty it'll be added to the page in a stylesheet
    "MobileMenuBreakpoint": 768,
    "MaxEventsShowing": 3,
    
    
    
    /*****
    	METHODS
    *****/
    
// BIG BANG
    "Init": function() {
    	$('[data-hide=""], [data-hide="!false"], [data-hide="true"]').remove();
        
        this.SetTemplateProps();
        this.RsMenu();
        this.MyStart();
        this.Search();
        this.Header();
        this.CustomDropdowns();
        this.ChannelBar();
        this.Body();
        this.Slideshow(); //move this into JsMediaQueries if it isn't present across all of the Breakpoints
        this.AppAccordion();
        this.GlobalIcons();
        this.Footer();
        this.JsMediaQueries();
        csGlobalJs.OpenInNewWindowWarning();
        
        if( this.DynamicStyles != '' ){ //Build out our stylesheet if any of the above items have added styles to it.
        	this.BuildStylesheet();
        }
        
        $(window)
        	.resize(function(){ 
                 CreativeTemplate.WindowResize(); 
                 CreativeTemplate.CalendarListView();
            })
            .scroll(function(){
                CreativeTemplate.WindowScroll();
            });
    },
    
    
// TOP LEVEL (Window/Document) EVENT HANDLERS
    "WindowLoad": function() {
        this.CalendarListView();
        csGlobalJs.OpenInNewWindowWarning();
        
          // MAKE NAVIGATION APPS ACCESSIBLE
        $('.ui-widget.app.navigation').csAccessibleNavigationApp();
    },
    "WindowResize": function() {
        this.JsMediaQueries();
        this.CalendarListView();
    },
    "WindowScroll": function(){ },
    
    
//TEMPLATE EVENT HANDLERS
    "JsMediaQueries": function() {
        if( this.GetBreakPoint() == 'desktop' ){
        	//Desktop
        	
        }else{
        	//BP's
        }
    },
    "AllyClick": function(event) {
        if(event.type == 'click') {
            return true;
        } else if(event.type == 'keydown' && (event.key == this.KeyCodes.space || event.key == this.KeyCodes.enter)) {
            return true;
        } else {
            return false;
        }
    },
    "CustomDropdowns": function() {
    	const _this = this;
        
    	/* customDropdowns
        **  Use: Sets up custom dropdown menus.
        */
    	$('.custom-dropdown input, .custom-dropdown a, .custom-dropdown select').attr('tabindex', -1);
        
        $('.custom-dropdown > button').off('click keydown').on('click keydown', function(e) { // Trigger the click event from the keyboard
            const code = e.key;
            
            // 13 = Return, 32 = Space
            if (e.type == 'click' || code === _this.KeyCodes.enter || code === _this.KeyCodes.space) {
            	clearTimeout(this.DropdownTimeout);
                
                if( $(this).attr('aria-expanded') == 'false' ){
                    _this.ClearDropdowns();
                    const dropdown = $('~ .dropdown', this);
                    dropdown.attr('aria-hidden', 'false').slideDown();
                    
                    _this.DropdownKeyboardFunctionality($(this), dropdown, _this.ClearDropdowns);
                    
                    $('a, input, select', dropdown).first().focus();
                    
                    $(this).attr('aria-expanded', 'true');
                    $(this).parent().addClass('openMenu');
                }else{
                    _this.ClearDropdowns();
                }
            }else if((code == 39 || code == 40) && $(this).attr('aria-expanded') == 'true'){
                e.preventDefault();
                if( $('~ .dropdown select', this).length <= 0 ){
                    $('~ .dropdown a', this).first().focus();
				}else{
                	$('~ .dropdown select', this).first().focus();
                }
            }else if( code == 9 ){
                if($(this).attr('aria-controls') != 'translate-option-list'){
                    _this.ClearDropdowns();
                }
            }
        });
        
        $('.custom-dropdown').hover(function(e){ //In
            clearTimeout(_this.DropdownTimeout);
        },function(e){ //Out
            //canDropDownHide = true;
            _this.DropdownTimeout = setTimeout(function(){
                _this.ClearDropdowns();
            }, 800);
        });
    }, /* Self-Made Dropdown - Main Function and Event Handlers*/
    "ClearDropdowns": function(){
    	$('.custom-dropdown.openMenu input, .custom-dropdown.openMenu a, .custom-dropdown.openMenu .custom-dropdown > button').attr('tabindex', -1);
        $('.custom-dropdown.openMenu .dropdown').attr('aria-hidden', 'true').slideUp();
        $('.custom-dropdown.openMenu > button').removeClass('acc-open').attr('aria-expanded', 'false');
        $('.custom-dropdown.openMenu li').removeClass('active-list-item');
        $('.custom-dropdown.openMenu').removeClass('openMenu');
    }, /* Self-Made Dropdown - Clears the dropdowns */
    "DropdownKeyboardFunctionality": function(callingTrigger, dropdown, clear){
    	const _this = this;
        
        $('a, input, select', dropdown).attr('tabindex', -1).keydown(function(e){
            const linkObj = $('a, input, select, button', dropdown);
            let action = '';
            
            switch(e.key){
                // CONSUME LEFT AND UP ARROWS
                case _this.KeyCodes.left:
                	if(!(this instanceof HTMLInputElement)) action = 'back';
                	break;
                case _this.KeyCodes.up:
                    action = 'back';
                	break;

                // CONSUME RIGHT AND DOWN ARROWS
                case _this.KeyCodes.right:
                	if(!(this instanceof HTMLInputElement)) action = 'next';
                    break;
                case _this.KeyCodes.down:
                    action = 'next';
                	break;
				
                case _this.KeyCodes.tab:
                	clear();
                	break;
                
                case _this.KeyCodes.esc:
                	e.preventDefault();
                    callingTrigger.focus();
                    clear();
                	break;
                
                default: //pass
                	break;
            }
            
            switch(action){ //this allows us to check the element type to perform different actions based on the key and the element type
            	case "back":
                	e.preventDefault();
                    e.stopImmediatePropagation();

                        // IS FIRST ITEM
                    if(linkObj.index($(this)) == 0) {
                        callingTrigger.focus();
                    } else {
                        linkObj[linkObj.index($(this)) - 1].focus();
                    }
                	break;
                
                case "next":
                	e.preventDefault();
                    e.stopImmediatePropagation();

                    // IS LAST ITEM
                    if(linkObj.index($(this)) == (linkObj.length - 1) ) {
                        // FOCUS FIRST ITEM
                        linkObj[0].focus();
                    } else {
                        // FOCUS NEXT ITEM
                        linkObj[linkObj.index($(this)) + 1].focus();
                    }
                	break;
                
                default://pass
                    break;
            }
        });
    }, /* Self Made Dropdown - a11y Event Handlers */
    
    
// SETTERS
    "SetTemplateProps": function() {
        // MYVIEW PAGE CHECK
        if($('#pw-body').length) this.IsMyViewPage = true;
    },
    "BuildStylesheet": function() {
    	let dynStyleSheet = document.createElement('style');
	
        if(dynStyleSheet) {
            dynStyleSheet.setAttribute('type', 'text/css');
            let head = document.getElementsByTagName('head')[0];

            if(head) {
                head.insertBefore(dynStyleSheet, head.childNodes[0]);
            }

            let rules = document.createTextNode(this.DynamicStyles);

            if(dynStyleSheet.styleSheet){ // IE
                dynStyleSheet.styleSheet.cssText = this.DynamicStyles;
            } else {
                dynStyleSheet.appendChild(rules);
            }
        }
    },
    
    
// GETTERS
    "GetBreakPoint": function() {
        return window.getComputedStyle(document.querySelector('body'), ':before').getPropertyValue('content').replace(/"|'/g, '');/*"*/
    },
    "HexToRgb": function(hex){
    	const r = parseInt((this.CutHex(hex)).substring(0,2),16);
		const g = parseInt((this.CutHex(hex)).substring(2,4),16);
        const b = parseInt((this.CutHex(hex)).substring(4,6),16);
        return r+', '+g+', '+b;
    },
    "CutHex": function(hex){
    	return (hex.charAt(0)=='#') ? hex.substring(1,7):hex;
    },
    
    
//TEMPLATE METHODS
    "MyStart": function() {
    	if($('.sw-mystart-button.signin').length > 0){ //SignIn
            $('#custom-mystart-signin').append($('.sw-mystart-button.signin').html());
            $('#custom-mystart-signin a').addClass('mystart-label');

            if($('.sw-mystart-button.register').length > 0){ //Register
                $('#custom-mystart-register').append($('.sw-mystart-button.register').html());
                $('#custom-mystart-register a').addClass('mystart-label');
            }
        }
        if($('.sw-mystart-button.manage').length > 0){ //Site Manager
            $('#gb-user-options ul').append('<li>' + $('.sw-mystart-button.manage').html() + '</li>');
            $('#sw-mystart-right > .sw-mystart-nav.sw-mystart-button.manage > a').remove();
        }
        if($('#sw-mystart-mypasskey').length > 0){ //Passkey
            $('#sw-mystart-mypasskey').removeClass('sw-mystart-button').prependTo('#gb-custom-mystart');
            $('#ui-btn-mypasskey').addClass('mystart-label');
            $('#ui-btn-mypasskey span').html('Passkeys');
        }
        if( $('#ui-btn-myview').length > 0 ){ //MyView
        	$('#gb-user-options ul').append('<li>' + $('.sw-mystart-button.pw').html() + '</li>');
            $('.sw-mystart-button.pw').remove();
        }
        
        $('#sw-myaccount-list li').appendTo('#gb-user-options ul');

        $('.sw-mystart-dropdown.schoollist ul li').clone().appendTo('#school-list');
        
        this.Translator();
    },
    "Header": function() {
        const homeLink = '<a href="/[$SiteAlias$]">';
        
        // ADD LOGO 
        if(<SWCtrl controlname="Custom" props="Name:showLogo" />) {
            let logoSrc = $.trim('<SWCtrl controlname="Custom" props="Name:logoSrc" />');
            const srcSplit = logoSrc.split('/');
            const srcSplitLen = srcSplit.length;
            
            if((logoSrc == '') || (srcSplit[srcSplitLen - 1] == 'default-man.jpg')) {
            	logoSrc = this.DefaultLogoSrc;
            }
            
            $('#header-logo').prepend(homeLink + '<img id="header-logo-image" src="' + logoSrc + '" alt="[$SiteName$] logo" /></a>');
            
            if('<SWCtrl controlname="Custom" props="Name:showSchoolName" />' == 'false'){ //logo only approach needs to have the logo wrapped in an h1
            	$('#header-logo > a').wrap('<h1 />');
            }else{
            	$('#header-logo-image').addClass('has-schoolname');
            }
        }else if('<SWCtrl controlname="Custom" props="Name:showSchoolName" />' == 'true'){
        	$('#header-logo h1').wrap(homeLink + '</a>');
        }else{
        	$('#header-logo').remove();
        }
    },
    "ChannelBar": function() {
    	$('.sw-channel-item').each(function(channelIndex, channel){
            if($('.sw-channel-dropdown', channel).children().length == 0){
                $('> a', channel).attr('aria-haspopup', false);
            }
        })
    
        //Overwrite system hover functionality for an animated version.
        $('#channel-navigation > .sw-channel-item > a').off('touchstart');
        $('.sw-channel-item').unbind('hover');
        $('.sw-channel-item').hover(function(){ //in
            const subList = $(this).children('ul');
            if ($.trim(subList.html()) !== '') {
                subList.slideDown(300, 'swing');
            }
            $(this).addClass('hover');
        }, function(){ //out
            $('.sw-channel-dropdown').slideUp(300, 'swing');
            $(this).removeClass('hover');
        });
    },
    "Body": function() {
        // AUTO FOCUS SIGN IN FIELD
        $('#swsignin-txt-username').focus();

        // CENTER SIGNED OUT MESSAGE AND SIGNIN BUTTON
        if($('div.ui-spn > div > p > span').text() == 'You have been signed out.'){
            $('div.ui-spn > div').css({'text-align' : 'center', 'padding' : '50px 0px 50px 0px'});
            //DOC add signed out breadcrumb
            $('.ui-breadcrumbs').append('<li class="ui-breadcrumb-last">Signed Out</li>');
        }
        
        $('#hp-content .hp-row').each(function(rowIndex, row){
        	if($('.hp-column', row).length > 0){
            	let rowApps = 0;
                let columns = 0;
                
            	$('.hp-column', row).each(function(columnIndex, column){
                	if($('.app', column).length > 0){
						rowApps++;
                        columns++;
                    }else{
                    	$(column).remove();
                    }
                });
                
                if(rowApps == 0){
                	$(row).remove();
                }else{
                	$(row).addClass('columns-' + columns);
                }
            }else{
            	if($('.app', row).length <= 0){
                	$(row).remove();
                }
            }
        });
        
		$('div.ui-widget.app').each(function(appIndex, app){
        	//more link
            if( $('.more-link', app).length > 0 ){
                $('.more-link', app).prependTo( $('.ui-widget-footer', app) );
                $('.more-link-under', app).parent().remove();
            }
            
            //view calendar link
            if( $('.view-calendar-link', app).length > 0 ){
                $('.view-calendar-link', app).prependTo( $('.ui-widget-footer', app) );
            }
            
            if( $('.app-level-social-follow', app).length > 0 && $('.app-level-social-follow', app).children().length > 0 ){
                $('.app-level-social-follow', app).prependTo( $('.ui-widget-footer', app) );
            }else{
            	$('.app-level-social-follow', app).remove();
            }

            //Hide empty footer
            if( $('.ui-widget-footer', app).children().not('.clear').length <= 0 ){
            	$('.ui-widget-footer', app).addClass('no-children').hide();
            }else{
            	$('.ui-widget-footer', app).addClass('has-children');
                $('.ui-widget-footer .clear', app).remove();
            }
        });

        // APPLY RESPONSIVE DIMENSIONS TO CONTENT IMAGES
        $('div.ui-widget.app .ui-widget-detail img')
            .not($('div.ui-widget.app.cs-rs-multimedia-rotator .ui-widget-detail img'))
            .not($('div.ui-widget.app.gallery.json .ui-widget-detail img'))
            .each(function() {
                if ($(this).attr('width') !== undefined && $(this).attr('height') !== undefined) { // IMAGE HAS INLINE DIMENSIONS
                    $(this).css({'display': 'inline-block', 'width': '100%', 'max-width': $(this).attr('width') + 'px', 'height': 'auto', 'max-height': $(this).attr('height') + 'px'});
                }
        });

        // ADJUST FIRST BREADCRUMB
        $('li.ui-breadcrumb-first > a > span').text('Home');
         

        // USE CHANNEL NAME FOR PAGELIST NAV HEADER IF ONE IS NOT PRESENT
        if(!$('div.sp.column.one .ui-widget-header h1').length) {
            $('div.sp.column.one .ui-widget-header').append('<h3>[$ChannelName$]</h3>');
        }
        
        
        //Customize Apps
        this.UpcomingEvents();
    },
    "Footer": function() {
    	const _this = this;
    	$('#back-to-top').on('click', function(){
        	_this.SmoothScroll('#gb-page', 0);
            $('#gb-custom-mystart .mystart-label:first').focus();
        });
    
    	//DOC get Schoolwires footer links
        $('a#feedback').text($('div#sw-footer-links li:eq(0) a').text()).attr('href', $('div#sw-footer-links li:eq(0) a').attr('href'));
        $('a#terms').text($('div#sw-footer-links li:eq(2) a').text()).attr('href', $('div#sw-footer-links li:eq(2) a').attr('href'));
        $('a#policy').text($('div#sw-footer-links li:eq(1) a').text()).attr('href', $('div#sw-footer-links li:eq(1) a').attr('href'));		
        $('p#copyright').text($('#sw-footer-copyright').text());

        $('div#bb-legal-logo').append( $('#sw-footer-logo').html() );
    },
    "Search": function() {
    	const defSearchText = 'Search...';
	
        const searchBox = $('#gb-search-input');
        
        searchBox
            .attr('placeholder', '')
            .val(defSearchText)
            .focus(function() {
                if($(this).val() == defSearchText) {
                    $(this).val('');
                }
            })
            .blur(function() {
                if($(this).val() == '') {
                    $(this).val(defSearchText);
                }
            });
            
        $('#gb-search-form').submit(function(e){
            e.preventDefault();
	
            if($('#gb-search-input').val() != '' && $('#gb-search-input').val() != defSearchText) {
                window.location.href = '/site/Default.aspx?PageType=6&SiteID=[$SiteID$]&SearchString=' + $('#gb-search-input').val();
            }
        });
        
        
        $('#search-trigger').on('click', function(e){
        	if($(this).attr('aria-expanded') == 'true'){
            	$(this).attr('aria-expanded', false);
                
                $('#' + $(this).attr('aria-controls')).attr('aria-hidden', true).hide(400);
            }else{
            	$(this).attr('aria-expanded', true);
                
                $('#' + $(this).attr('aria-controls')).attr('aria-hidden', false).show(400);
                
				searchBox.focus();
            }
        });
    },
    "SmoothScroll": function(to, offset){
    	/*
        ** smoothScroll
        ** Use: This will smoothly scroll to the id that was passed to the function.
        ** Return: N/A
        ** Parameter(s): to (jQuery "#id") The id of the section to be scolled to.
        ** 		offset (int) The amount of space you want to be left on the top of the element after scroll (in a whole number int value)
        */
    	if( to.indexOf('#') < 0 ){ to = '#' + to; } //makes sure we have a valid id accessor string
        const target = $(to);

        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top - offset //this offset accounts for the fixed menu on the top
            }, 800, function(){
                $('html, body').stop(true, false);
            });
            return false;
        }
    },
    
    
// PLUGIN METHODS
    "Slideshow": function() {
    	const _this = this;
        
        if('<SWCtrl controlname="Custom" props="Name:heroOption" />' == 'Streaming Video'){
        	$('#hp-slideshow .region').remove();
            
        	$("#hp-slideshow").fullScreenRotator({
                "videoSource" : '<SWCtrl controlname="Custom" props="Name:streamingVideoVendor" />', // OPTIONS ARE: youtube, vimeo
                "videoID": '<SWCtrl controlname="Custom" props="Name:streamingVideoId" />', // YouTube and Vimeo ids are set as default in the script
                "fullScreenBreakpoints" : [960], // OPTIONS ARE [960, 768, 640, 480, 320]
                "showControls" : true,
                "showMobileBackgroundPhoto" : false,
                "onReady" : function() { 
                    //add the controls
                    $('#hp-slideshow').append('<div class="mmg-description-wrapper streaming-video-description-wrapper flex-cont fd-col jc-sb">\
                                                  <div class="mmg-description-outer streaming-video-description-outer">\
                                                      <div class="streaming-video-controls mmg-controls flex-cont ai-c jc-c">\
                                                          <button id="streaming-video-state-control" class="mmg-control play-pause playing" aria-label="Pause the streaming video"><i class="dev-icon-pause"></i></button>\
                                                          <button id="streaming-video-audio-control" class="mmg-control audio muted" aria-label="Unmute the streaming video"><i class="dev-icon-audio-off"><span class="path1"></span><span class="path2"></span></i></button>\
                                                          <button id="streaming-video-popout" class="mmg-control popout" aria-label="Show the streaming video in a modal"><i class="dev-icon-expand"></i></button>\
                                                      </div>\
                                                   </div>\
                                               </div>');

                    $("#streaming-video-audio-control").click(function(e){
                        if( $(this).hasClass('muted') ){
                            $(this)
                                .attr('aria-label', 'Mute streaming video')
                                .removeClass('muted').addClass('unmuted');
                            $('.cs-fullscreen-video-button.unmute-button').click();
                        }else{
                            $(this)
                                .attr('aria-label', 'Unmute streaming video')
                                .removeClass('unmuted').addClass('muted');
                            $('.cs-fullscreen-video-button.mute-button').click();
                        }
                    });
                    
                    if( $('.ios').length > 0 ){
                        $("#streaming-video-state-control")
                              .attr('aria-label', 'Play streaming video')
                              .removeClass('playing')
                              .addClass('paused')
                              .click();
                    }

                    $("#streaming-video-state-control").click(function(e){
                        if( $(this).hasClass('playing') ){
                            $(this)
                                .attr('aria-label', 'Play streaming video')
                                .removeClass('playing').addClass('paused');
                            $('.cs-fullscreen-video-button.pause-button').click();
                        }else{
                            $(this)
                                .attr('aria-label', 'Pause streaming video')
                                .removeClass('paused').addClass('playing');
                            $('.cs-fullscreen-video-button.play-button').click();
                        }
                    });
                    
                    $('#streaming-video-popout').click(function(e){
                        $('.cs-fullscreen-video-button.watch-video-button').click();
                        $('body').addClass('video-popup-showing');
                    });

                    $('body').on('click keydown', '.cs-fullscreen-video-close', function(e){
                        if(e.type == "click" || e.key == _this.KeyCodes.enter){
                            $('body').removeClass('video-popup-showing');
                            
                            setTimeout(function() {
                                // REMOVE THE VIDEO CONTAINER COMPLETELY
                                $("#cs-fullscreen-video-outer .cs-fullscreen-video-window").remove();

                                // FOCUS WATCH VIDEO LINK
                                $("#streaming-video-popout").focus();
                            }, 1100);

						}
                    });
                
                	let streamingDesc = '';
                    //Title
                    if('<SWCtrl controlname="Custom" props="Name:streamingVideoTitle" />' != ''){
                    	streamingDesc += '<h2 class="mmg-description-title"><SWCtrl controlname="Custom" props="Name:streamingVideoTitle" /></h2>';
                    }
                    
                    //Caption
                    if('<SWCtrl controlname="Custom" props="Name:streamingVideoCaption" />' != ''){
                    	streamingDesc += '<p class="mmg-description-caption"><SWCtrl controlname="Custom" props="Name:streamingVideoCaption" /></p>';
                    }
                    
                    //Link
                    if('<SWCtrl controlname="Custom" props="Name:streamingVideoLinkUrl" />' != ''){
                    	streamingDesc += '<div class="mmg-description-links"><a class="mmg-description-link" href="<SWCtrl controlname="Custom" props="Name:streamingVideoLinkUrl" />" target="<SWCtrl controlname="Custom" props="Name:streamingVideoLinkTarget" />"><span><SWCtrl controlname="Custom" props="Name:streamingVideoLinkText" /></span></a></div>';
                    }
                    
                    if(streamingDesc != ''){
						$('#hp-slideshow .streaming-video-description-outer').prepend('<div class="mmg-description">' + streamingDesc + '</div>');
                    }
                    
                    _this.ManageHeroTitle($('#hp-slideshow .streaming-video-description-wrapper'));
                    
                    $('#hp-slideshow').addClass('loaded');
                    
                    _this.JsMediaQueries();
                },
                "onStateChange" : function() { }
            });
		}else{
        	//will combine all of the MMGs in Region 10 into the first MMG 
            const mmgs = $('#sw-content-container10 .ui-widget.app.multimedia-gallery').length;

            if(mmgs) {
                let mmg = eval("multimediaGallery" + $("#sw-content-container10.ui-hp div.ui-widget.app.multimedia-gallery:first").attr('data-pmi'));

                if(mmgs > 1){
                    mmg.records.splice(-1, 1);

                    $("#sw-content-container10.ui-hp div.ui-widget.app.multimedia-gallery").not($("#sw-content-container10.ui-hp div.ui-widget.app.multimedia-gallery:first")).each(function() {
                        const dataArray = eval("multimediaGallery" + $(this).attr('data-pmi'));
                        dataArray.records.splice(-1, 1);

                        $.each(dataArray.records, function(index, record) {
                            mmg.records.push(record);
                        });

                        $(this).remove();
                    });

                    mmg.records.push({});
                }

                mmg.props.defaultGallery = false;

                $('#sw-content-container10 .ui-widget.app.multimedia-gallery:first').csMultimediaGallery({
                    'efficientLoad' : true,
                    'imageWidth' : 1680,
                    'imageHeight' : 693,
                    'mobileDescriptionContainer': [960, 768, 640, 480, 320], // [960, 768, 640, 480, 320]
                    'galleryOverlay' : false,
                    'linkedElement' : [], // ["image", "title", "overlay"]
                    'playPauseControl' : true,
                    'backNextControls' : true,
                    'bullets' : false,
                    'thumbnails' : false,
                    'thumbnailViewerNum': [4, 4, 3, 3, 2], // NUMERICAL - [960 view, 768 view, 640 view, 480 view, 320 view]
                    'autoRotate' : true,
                    'hoverPause' : true,
                    'transitionType' : 'fade', // fade, slide, custom
                    'transitionSpeed' : 1.5,
                    'transitionDelay' : 4,
                    'fullScreenRotator' : false,
                    'fullScreenBreakpoints' : [960], // NUMERICAL - [960, 768, 640, 480, 320]
                    'onImageLoad' : function(props) {}, // props.element, props.recordIndex, props.mmgRecords
                    'allImagesLoaded' : function(props) {}, // props.element, props.mmgRecords
                    'onTransitionStart' : function(props) {}, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.nextRecordIndex, props.nextGalleryIndex, props.mmgRecords
                    'onTransitionEnd' : function(props) {}, // props.element, props.currentRecordIndex, props.currentGalleryIndex, props.mmgRecords
                    'allLoaded' : function(props) {
                        $('.mmg-description-outer', props.element).wrap('<div class="mmg-description-wrapper flex-cont fd-col jc-sb" />');

                        if( $('.mmg-control', props.element).length > 0 ){
                            $('.mmg-description-outer', props.element).append('<div class="mmg-controls flex-cont ai-c jc-c"></div>');

                            $('.mmg-controls', props.element).append($('#hp-slideshow .mmg-control.back'));
                            $('.mmg-controls', props.element).append($('#hp-slideshow .mmg-control.play-pause'));
                            $('.mmg-controls', props.element).append($('#hp-slideshow .mmg-control.next'));
                        }

                        //add the mmg banner title and logo if it's setup
                        _this.ManageHeroTitle($('.mmg-description-wrapper', props.element));
                        
                        _this.JsMediaQueries();

                    }, // props.element, props.mmgRecords
                    'onWindowResize': function(props) {} // props.element, props.mmgRecords
                });
            }
    	}
    },
    "RsMenu": function(schoolList) {
        
        $.csRsMenu({
            'breakPoint' : 1023, // SYSTEM BREAK POINTS - 768, 640, 480, 320
            'slideDirection' : 'left-to-right', // OPTIONS - left-to-right, right-to-left
            'menuButtonParent' : '#gb-channel-list',
            'menuBtnText' : 'MENU',
            'showSchools' : true,
            'schoolMenuText': 'Schools',
            'showTranslate' : true,
            'translateMenuText': 'Translate',
            'translateVersion': 2, // 1 = FRAMESET, 2 = BRANDED
            'showAccount': true,
            'accountMenuText': 'User Options',
            'usePageListNavigation': true,
            'extraMenuOptions': {}, 
            'siteID': '[$siteID$]',
            'allLoaded': function() {}
        });
    },
    "Translator": function() {
    	$('#gb-translate-menu').creativeTranslate({
            'type': 2, // 1 = FRAMESET, 2 = BRANDED, 3 = API
            'advancedOptions': {
                'addMethod': 'append',
                'dropdownHandleText': 'Translate',
                'removeBrandedDefaultStyling': true,
                'useTranslatedNames': true, // ONLY FOR FRAMESET AND API VERSIONS - true = TRANSLATED LANGUAGE NAME, false = ENGLISH LANGUAGE NAME
            },
            'translateLoaded': function() { } // CALLBACK TO RUN YOUR CUSTOM CODE AFTER EVERYTHING IS READY
        });
    },
    "AppAccordion": function(){
        $('.sp-column.one').csAppAccordion({
            'accordionBreakpoints' : [768, 640, 480, 320]
        });
    },
    "GlobalIcons": function(){
    	$('#gb-global-icons').creativeIcons({
            'iconNum'       : '<SWCtrl controlname="Custom" props="Name:globalNumOfIcons" />',
            'defaultIconSrc': '',
            'icons'         : [
                {
                    'image': '<SWCtrl controlname="Custom" props="Name:globalIcon1Image" />',
                    'showText': <SWCtrl controlname="Custom" props="Name:globalIcon1ShowText" />,
                    'text': '<SWCtrl controlname="Custom" props="Name:globalIcon1Text" />',
                    'url': '<SWCtrl controlname="Custom" props="Name:globalIcon1Link" />',
                    'target': '<SWCtrl controlname="Custom" props="Name:globalIcon1Target" />'
                },
                {
                    'image': '<SWCtrl controlname="Custom" props="Name:globalIcon2Image" />',
                    'showText': <SWCtrl controlname="Custom" props="Name:globalIcon2ShowText" />,
                    'text': '<SWCtrl controlname="Custom" props="Name:globalIcon2Text" />',
                    'url': '<SWCtrl controlname="Custom" props="Name:globalIcon2Link" />',
                    'target': '<SWCtrl controlname="Custom" props="Name:globalIcon2Target" />'
                },
                {
                    'image': '<SWCtrl controlname="Custom" props="Name:globalIcon3Image" />',
                    'showText': <SWCtrl controlname="Custom" props="Name:globalIcon3ShowText" />,
                    'text': '<SWCtrl controlname="Custom" props="Name:globalIcon3Text" />',
                    'url': '<SWCtrl controlname="Custom" props="Name:globalIcon3Link" />',
                    'target': '<SWCtrl controlname="Custom" props="Name:globalIcon3Target" />'
                },
                {
                    'image': '<SWCtrl controlname="Custom" props="Name:globalIcon4Image" />',
                    'showText': <SWCtrl controlname="Custom" props="Name:globalIcon4ShowText" />,
                    'text': '<SWCtrl controlname="Custom" props="Name:globalIcon4Text" />',
                    'url': '<SWCtrl controlname="Custom" props="Name:globalIcon4Link" />',
                    'target': '<SWCtrl controlname="Custom" props="Name:globalIcon4Target" />'
                },
                {
                    'image': '<SWCtrl controlname="Custom" props="Name:globalIcon5Image" />',
                    'showText': <SWCtrl controlname="Custom" props="Name:globalIcon5ShowText" />,
                    'text': '<SWCtrl controlname="Custom" props="Name:globalIcon5Text" />',
                    'url': '<SWCtrl controlname="Custom" props="Name:globalIcon5Link" />',
                    'target': '<SWCtrl controlname="Custom" props="Name:globalIcon5Target" />'
                },
                {
                    'image': '<SWCtrl controlname="Custom" props="Name:globalIcon6Image" />',
                    'showText': <SWCtrl controlname="Custom" props="Name:globalIcon6ShowText" />,
                    'text': '<SWCtrl controlname="Custom" props="Name:globalIcon6Text" />',
                    'url': '<SWCtrl controlname="Custom" props="Name:globalIcon6Link" />',
                    'target': '<SWCtrl controlname="Custom" props="Name:globalIcon6Target" />'
                },
                {
                    'image': '<SWCtrl controlname="Custom" props="Name:globalIcon7Image" />',
                    'showText': <SWCtrl controlname="Custom" props="Name:globalIcon7ShowText" />,
                    'text': '<SWCtrl controlname="Custom" props="Name:globalIcon7Text" />',
                    'url': '<SWCtrl controlname="Custom" props="Name:globalIcon7Link" />',
                    'target': '<SWCtrl controlname="Custom" props="Name:globalIcon7Target" />'
                },
                {
                    'image': '<SWCtrl controlname="Custom" props="Name:globalIcon8Image" />',
                    'showText': <SWCtrl controlname="Custom" props="Name:globalIcon8ShowText" />,
                    'text': '<SWCtrl controlname="Custom" props="Name:globalIcon8Text" />',
                    'url': '<SWCtrl controlname="Custom" props="Name:globalIcon8Link" />',
                    'target': '<SWCtrl controlname="Custom" props="Name:globalIcon8Target" />'
                }
            ],
            'siteID'        : '[$SiteID$]',
            'siteAlias'     : '[$SiteAlias$]',
            'calendarLink'  : '[$SiteCalendarLink$]',
            'contactEmail'  : '[$SiteContactEmail$]',
            'allLoaded'     : function(){ }
        });
    },
    
    
// APP METHODS
    "CalendarListView": function() {
        if($('.ui-widget.app.calendar').length) {
            // AUTO CLICK FUNCTION FOR SCHOOLWIRES CALENDAR LIST VIEW FOR SMALL MOBILE DEVICES
            if(!$('.ui-widget.app.calendar #calendarlist-pnl-specialview.loaded').length){
                $('a.ui-btn-toggle.list').click();
                $('a.ui-btn-toggle.month').click();
                $('.ui-widget.app.calendar #calendarlist-pnl-specialview').addClass('loaded');
            }

            switch(this.GetBreakPoint()) {
                case 'desktop': 
                case '768': 
                case '640':
                    $('.ui-widget.app.calendar #calendar').removeClass('ui-helper-hidden');
                    $('.ui-widget.app.calendar #calendarlist-pnl-specialview').addClass('ui-helper-hidden');
                break;
                case '480': 
                case '320':
                    $('.ui-widget.app.calendar #calendar').addClass('ui-helper-hidden');
                    $('.ui-widget.app.calendar #calendarlist-pnl-specialview').removeClass('ui-helper-hidden');
                    $('.ui-btn-toggle.month').removeClass('active');
                    $('.ui-btn-toggle.list').addClass('active');
                break;
            }
        }
    },
    "UpcomingEvents": function(){
    	if (!$('.upcomingevents .joel-month').length) {
        	const _this = this;
            
            $('.upcomingevents').modEvents({
                columns     : 'yes',
                monthLong   : 'no',
                dayWeek     : 'no'
            });
            let countLi = 0;
            $('.upcomingevents .ui-articles .ui-article').each(function(){
                if (!$(this).find('h1.ui-article-title.sw-calendar-block-date').size()){
                    $(this).parent().prev().children().children().next().append($(this).html());
                    $(this).parent().remove();
                };
            });
            
            $(".upcomingevents .ui-articles .ui-article").each(function(articleIndex, article){
                if($('.ui-article-description', article).length > _this.MaxEventsShowing){
                	//add the calendar link plus sign
					$(article).append('<a class="custom-view-more flex-cont ai-c jc-c" href="[$SiteCalendarLink$]" aria-label="View more events"><i class="dev-icon-plus"></i><span>View more events</span></a>');
                	$('.ui-article-description:gt(' + (_this.MaxEventsShowing - 1) + ')', article).remove();
                }
                
            	$('.ui-article-description', article).each(function(descIndex, desc){
                	if($('.sw-calendar-block-time', desc).length <= 0){
                    	$(desc).append('<span class="sw-calendar-block-time">All Day</span>');
                    }else{
                    	$(desc).append($('.sw-calendar-block-time', desc));
                    }
                });
            });
        }
    }
};

$(function() {
	CreativeTemplate.Init(); //tip of the iceberg
});

$(window).on('load', function(e){
	CreativeTemplate.WindowLoad();
});
